FROM nginx:1.14.1-alpine

CMD ["nginx", "-g", "daemon off;"]
